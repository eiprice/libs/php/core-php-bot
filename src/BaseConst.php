<?php


namespace Eiprice\Core;

use Eiprice\Core\Exceptions\InvalidEnumValueException;
use \ReflectionClass;

/**
 * Class Enum
 * @package Eiprice\Core
 */
abstract class BaseConst
{
    protected $data;

    private function __construct()
    {
        // Just for error
    }

    public static function getConstants() : array
    {
        $reflectionClass = new ReflectionClass(static::class);
        return $reflectionClass->getConstants();
    }

    public static function getKey($value)
    {
        return array_search($value, static::getConstants());;
    }

    public static function getLabel($key)
    {
        return static::getConstants()[$key];
    }

    public static function checkValue($value) : void
    {
        $enum_name = static::class;
        if ( !in_array($value, self::getConstants())){
            throw new InvalidEnumValueException("{$value} doesn't exist int {$enum_name}");
        }
    }
}
