<?php


namespace Eiprice\Core\Collection;


use ArrayIterator;
use Eiprice\Core\Contract\IDataCrawler;

/**
 * Class DataCrawlerCollection
 * @package Eiprice\Core\Collection
 */
class DataCrawlerCollection extends ArrayIterator
{
    /**
     * DataCrawlerCollection constructor
     * @param IDataCrawler ...$shipments
     */
    public function __construct(IDataCrawler ...$dataCrawler)
    {
        parent::__construct($dataCrawler);
    }

    /**
     * @return Shipment
     */
    public function current() : IDataCrawler
    {
        return parent::current();
    }

    /**
     * @param $offset
     * @return Shipment
     */
    public function offsetGet($offset) : IDataCrawler
    {
        return parent::offsetGet($offset);
    }

    /**
     * @param IDataCrawler $shipment
     */
    public function add(IDataCrawler $dataCrawler) : void
    {
        $this->getInnerIterator()->append($dataCrawler);
    }

    /**
     * @param int $key
     * @param Shipment $shipment
     */
    public function set(int $key, IDataCrawler $dataCrawler) : void
    {
        $this->getInnerIterator()->offsetSet($key, $dataCrawler);
    }

    /**
     * @return IDataCrawler
     */
    public function next() : ?IDataCrawler
    {
        return parent::next();
    }
}
