<?php


namespace Eiprice\Core\Collection;


use ArrayIterator;
use Eiprice\Core\Contract\IDataSpider;

/**
 * Class DataSpiderCollection
 * @package Eiprice\Core\Collection
 */
class DataSpiderCollection  extends ArrayIterator
{
    /**
     * DataSpiderCollection constructor
     * @param IDataSpider ...$shipments
     */
    public function __construct(IDataSpider ...$PageProcessors)
    {
        parent::__construct($PageProcessors);
    }

    /**
     * @return Shipment
     */
    public function current() : IDataSpider
    {
        return parent::current();
    }

    /**
     * @param $offset
     * @return Shipment
     */
    public function offsetGet($offset) : IDataSpider
    {
        return parent::offsetGet($offset);
    }

    /**
     * @param IDataSpider $shipment
     */
    public function add(IDataSpider $PageProcessor) : void
    {
        $this->getInnerIterator()->append($PageProcessor);
    }

    /**
     * @param int $key
     * @param Shipment $shipment
     */
    public function set(int $key, IDataSpider $PageProcessor) : void
    {
        $this->getInnerIterator()->offsetSet($key, $PageProcessor);
    }

    /**
     * @return IDataSpider
     */
    public function next() : ?IDataSpider
    {
        return parent::next();
    }
}
