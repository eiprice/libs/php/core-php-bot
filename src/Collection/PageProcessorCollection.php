<?php


namespace Eiprice\Core\Collection;

use \ArrayIterator;
use Eiprice\Core\Spiders\Crawler\PageProcessor;

/**
 * Class PageProcessorCollection|PageProcessor[]
 * @package Eiprice\Core\Collection
 */
class PageProcessorCollection  extends ArrayIterator
{
    /**
     * PageProcessorCollection constructor
     * @param PageProcessor ...$shipments
     */
    public function __construct(PageProcessor ...$PageProcessors)
    {
        parent::__construct($PageProcessors);
    }

    /**
     * @return Shipment
     */
    public function current() : PageProcessor
    {
        return parent::current();
    }

    /**
     * @param $offset
     * @return Shipment
     */
    public function offsetGet($offset) : PageProcessor
    {
        return parent::offsetGet($offset);
    }

    /**
     * @param PageProcessor $PageProcessor
     */
    public function add(PageProcessor $PageProcessor) : void
    {
        $this->append($PageProcessor);
    }

    /**
     * @param int $key
     * @param Shipment $shipment
     */
    public function set(int $key, PageProcessor $PageProcessor) : void
    {
        $this->getInnerIterator()->offsetSet($key, $PageProcessor);
    }

    /**
     * @return PageProcessor
     */
    public function next() : ?PageProcessor
    {
        return parent::next();
    }
}
