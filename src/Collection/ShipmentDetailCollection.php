<?php


namespace Eiprice\Core\Collection;


class ShipmentDetailCollection extends ArrayIterator
{
    /**
     * ShipmentDetailCollection constructor.
     * @param Shipment ...$shipments
     */
    public function __construct(ShipmentDetail ...$shipments)
    {
        parent::__construct($shipments);
    }

    /**
     * @return Shipment
     */
    public function current() : ShipmentDetail
    {
        return parent::current();
    }

    /**
     * @param $offset
     * @return Shipment
     */
    public function offsetGet($offset) : ShipmentDetail
    {
        return parent::offsetGet($offset);
    }

    /**
     * @param Shipment $shipment
     */
    public function add(Shipment $shipment) : void
    {
        $this->getInnerIterator()->append($shipment);
    }

    /**
     * @param int $key
     * @param Shipment $shipment
     */
    public function set(int $key, Shipment $shipment) : void
    {
        $this->getInnerIterator()->offsetSet($key, $shipment);
    }

    /**
     * @return Shipment
     */
    public function next() : ?ShipmentDetail
    {
        return parent::next();
    }
}
