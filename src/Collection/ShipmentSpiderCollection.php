<?php


namespace Eiprice\Core\Collection;


/**
 * Class ShipmentSpiderCollection
 * @package Eiprice\Core
 */
class ShipmentSpiderCollection extends ArrayIterator
{
    /**
     * ShipmentSpiderCollection constructor.
     * @param Shipment ...$shipments
     */
    public function __construct(Shipment ...$shipments)
    {
        parent::__construct($shipments);
    }

    /**
     * @return Shipment
     */
    public function current() : Shipment
    {
        return parent::current();
    }

    /**
     * @param $offset
     * @return Shipment
     */
    public function offsetGet($offset) : Shipment
    {
        return parent::offsetGet($offset);
    }

    /**
     * @param Shipment $shipment
     */
    public function add(Shipment $shipment) : void
    {
        $this->getInnerIterator()->append($shipment);
    }

    /**
     * @param int $key
     * @param Shipment $shipment
     */
    public function set(int $key, Shipment $shipment) : void
    {
        $this->getInnerIterator()->offsetSet($key, $shipment);
    }

    /**
     * @return Shipment
     */
    public function next() : ?Shipment
    {
        return parent::next();
    }
}
