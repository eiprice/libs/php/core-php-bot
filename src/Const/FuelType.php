<?php


namespace Eiprice\Core\Enums;


use Eiprice\Core\BaseConst;

/**
 * Class FuelType
 * @package Eiprice\Core\Enums
 */
final class FuelType extends BaseConst
{
    const ALCOHOL = 'Alcool';
    const DIESEL = 'Diesel';
    const FLEX = 'Flex';
    const GASOLINE = 'Gasolina';
    const GNV = 'GNV';
    const ELETRIC_HIBRID = 'Elétrico / Hibrido';
    const MULTIFLEX = 'Alcool / Gasolina / GNV';
}
