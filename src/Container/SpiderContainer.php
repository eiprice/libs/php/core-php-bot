<?php


namespace Eiprice\Core\Container;

use \DOMDocument;
use \DOMXpath;
use QL\QueryList;
use Eiprice\Core\Contract\ISpiderContainer;
use stdClass;

/**
 * Class SpiderContainer
 * @package Eiprice\Core
 */
class SpiderContainer
    implements ISpiderContainer
{
    /**
     * @var string
     */
    protected $content = '';
    protected $headers = [];
    protected $proxy = '';

    /**
     * @var DOMDocument
     */
    protected $dom = null;

    /**
     * @var string
     */
    protected $url = '';

    /**
     * @param string $content
     */
    public function setContent($content = '')
    {
        $this->content = $content;
    }

    /**
     * @param string $content
     */
    public function setHeaders($headers = [])
    {
        $this->headers = $headers;
    }

    public function get_headers(): array
    {
        return $this->headers;
    }


    /**
     * @param string $content
     */
    public function setUrl($url = '')
    {
        $this->url = $url;
    }

    /**
     * @return string
     */
    public function get_content()
    {
        return $this->content;
    }

    public function set_proxy($proxy = '')
    {
        $this->proxy = $proxy;
    }

    public function get_proxy()
    {
        return $proxy;
    }

    /**
     * @return string
     */
    public function get_url()
    {
        return $this->url;
    }


    protected function load_dom()
    {
        $this->dom = new DOMDocument('1.0', 'UTF-8');

        // set error level
        $internalErrors = libxml_use_internal_errors(true);

        $html = mb_convert_encoding($this->get_content(), 'ISO-8859-1' );


        // load HTML
        $this->dom->loadHTML($html);

        // Restore error level
        libxml_use_internal_errors($internalErrors);
    }

    /**
     * @param $query_str
     * @return \DOMNodeList|false
     */
    public function xpath($query_str)
    {
        if ( $this->dom == null){
            $this->load_dom();
        }
        $xpath = new DOMXpath($this->dom);
        return $xpath->query($query_str);
    }


    /**
     * @param $pattern
     * @param string $matches
     * @return false|int
     */
    public function preg_match($pattern, &$matches = null, $flags = 0, $offset = 0)
    {
        return preg_match(
            $pattern,
            $this->get_content(),
            $matches,
            $flags,
            $offset
        );
    }

    public function preg_match_all($pattern, &$matches = '', $flags = PREG_PATTERN_ORDER, $offset = 0)
    {
        return preg_match_all(
            $pattern,
            $this->get_content(),
            $matches,
            $flags,
            $offset
        );
    }

    /**
     * @return QueryList
     */
    public function get_ql() : QueryList
    {
        return QueryList::html($this->get_content());
    }

    /**
     * @return arra|StdClass
     */
    public function json()
    {
        return json_decode($this->get_content());
    }

    /**
     * @return bool
     */
    public function isJson() : bool
    {
        return json_decode($this->get_content())!==false;
    }
}
