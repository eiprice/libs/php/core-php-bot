<?php


namespace Eiprice\Core\Contract;


interface IDataCrawler extends IDataSpider
{
    public function setSlugs(array $slugs) : void;
    public function getSlugs() : array;
}
