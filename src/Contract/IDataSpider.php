<?php


namespace Eiprice\Core\Contract;


interface IDataSpider extends \JsonSerializable
{
    public function set_proxy($proxy) : void;
    public function get_proxy() : string;
}
