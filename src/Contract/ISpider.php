<?php


namespace Eiprice\Core\Contract;


/**
 * Interface ISpider
 * @package Eiprice\Core\Contract
 */
interface ISpider
{
    /**
     * @return array
     */
    public function available_drivers() : array;

    /**
     * @return array
     */
    public function pre_requests() : array;

    /**
     * @return array
     */
    public function post_requests() : array;

    /**
     * @return array
     */
    public function custom_headers() : array;
}
