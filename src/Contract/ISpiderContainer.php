<?php

namespace Eiprice\Core\Contract;


use DOMDocument;
use DOMXPath;
use QL\QueryList;
use \StdClass;

/**
 * Interface ISpiderContainer
 * @package Eiprice\Core\Contract
 */
interface ISpiderContainer
{

    /**
     * @param string $url
     * @return mixed
     */
    public function setUrl($url = '');


    /**
     * @param array $headers
     * @return mixed
     */
    public function setHeaders($headers = []);


    /**
     * @return array
     */
    public function get_headers() : array;


    /**
     * @param string $content
     * @return mixed
     */
    public function setContent($content = '');

    /**
     * @return mixed
     */
    public function get_content();

    /**
     * @param string $proxy
     * @return mixed
     */
    public function set_proxy($proxy = '');

    /**
     * @return mixed
     */
    public function get_proxy();

    /**
     * @return mixed
     */
    public function get_url();

    /**
     * @param $query_str
     * @return \DOMNodeList|false
     */
    public function xpath($query_str);

    /**
     * @param $pattern
     * @param string $matches
     * @return mixed
     */
    public function preg_match($pattern, &$matches = '');

    /**
     * @return array|StdClass
     */
    public function json();

    /**
     * @return bool
     */
    public function isJson() : bool;

    /**
     *
     * @return QueryList
     */
    public function get_ql() : QueryList;
}
