<?php


namespace Eiprice\Core\ScrapDetail;


use Eiprice\Core\Contract\IDataCrawler;

/**
 * Class CustomDetail
 * @package Eiprice\Core\ScrapDetail
 */
class CustomDetail implements IDataCrawler
{
    protected $data = [];
    protected $slugs = [];

    /**
     * @param $name
     * @param $value
     */
    public function __set($name, $value)
    {
        $this->data[$name] = $value;
    }

    /**
     * @param $name
     * @return mixed
     */
    public function __get($name)
    {
        return $this->data[$name];
    }

    /**
     * @param array $slugs
     */
    public function setSlugs(array $slugs): void
    {
        $this->slugs = $slugs;
    }

    /**
     * @return array
     */
    public function getSlugs(): array
    {
        return $this->slugs;
    }


    /**
     * @param $proxy
     */
    public function set_proxy($proxy): void
    {
        $this->data['proxy'] = $proxy;
    }

    /**
     * @return string
     */
    public function get_proxy(): string
    {
        return $this->data['proxy'];
    }

    /**
     * @return array|mixed
     */
    public function jsonSerialize()
    {
        return $this->data;
    }


}
