<?php

namespace Eiprice\Core\ScrapDetail;

use Eiprice\Core\Contract\IDataSpider;

/**
 * Class ProductDetail
 * @package Eiprice\Core\ScrapDetail
 */
class ProductDetail implements IDataSpider
{
    protected $url;
    protected $ean;
    protected $sku;
    protected $types;
    protected $brand;
    protected $model;
    protected $description;
    protected $regular_price;
    protected $sales_price;
    protected $price_billet;
    protected $offer_type;
    protected $price_offer;
    protected $installments_price;
    protected $installments;
    protected $installment_amount;
    protected $interest_rate;
    protected $stock;
    protected $stock_quantity;
    protected $status;
    protected $obs;
    protected $object;
    protected $image;
    protected $seller;
    protected $unit = 1;
    protected $extra_data;

    protected $proxy;

    public function set_object($object)
    {
        $this->object = $object;
    }
    public function get_object()
    {
        return $this->object;
    }

    public function set_image(string $image)
    {
        $this->image = $image;
    }
    public function get_image() : string
    {
        return $this->image;
    }


    public function set_seller(string $seller)
    {
        $this->seller = $seller;
    }
    public function get_seller() : ?string
    {
        return $this->seller;
    }



    /**
     * @param $array
     * @return \Eiprice\Core\ProductDetail
     */
    public static function fromArray($array)
    {
        $instance = new self();
        $instance->set_brand(isset($array['brand'])? $array['brand'] : '');
        $instance->set_description(isset($array['description'])? $array['description'] : '');
        $instance->set_ean(isset($array['ean'])? $array['ean'] : '');
        $instance->set_model(isset($array['model'])? $array['model'] : '');
        $instance->set_sku(isset($array['sku'])? $array['sku'] : '');
        $instance->set_types(isset($array['types'])? $array['types'] : '');
        $instance->set_image(isset($array['image'])? $array['image'] : '');

        return $instance;
    }

    /**
     * @return mixed
     */
    public function get_ean()
    {
        return $this->ean;
    }

    /**
     * @param mixed $ean
     */
    public function set_ean($ean): void
    {
        $this->ean = $ean;
    }

    /**
     * @return mixed
     */
    public function get_url() : ?string
    {
        return $this->url;
    }

    /**
     * @param mixed $url
     */
    public function set_url($url): void
    {
        $this->url = $url;
    }

    /**
     * @return mixed
     */
    public function get_sku()
    {
        return $this->sku;
    }

    /**
     * @param mixed $sku
     */
    public function set_sku($sku): void
    {
        $this->sku = $sku;
    }

    /**
     * @return mixed
     */
    public function get_types()
    {
        return $this->types;
    }

    /**
     * @param mixed $types
     */
    public function set_types($types): void
    {
        $this->types = $types;
    }

    /**
     * @return mixed
     */
    public function get_brand()
    {
        return $this->brand;
    }

    /**
     * @param mixed $brand
     */
    public function set_brand($brand): void
    {
        $this->brand = $brand;
    }

    /**
     * @return mixed
     */
    public function get_model()
    {
        return $this->model;
    }

    /**
     * @param mixed $model
     */
    public function set_model($model): void
    {
        $this->model = $model;
    }

    /**
     * @return mixed
     */
    public function get_description()
    {
        return $this->description;
    }

    public function set_extra($extra_data) : void
    {
        $this->extra_data = $extra_data;
    }

    public function get_extra()
    {
        return $this->extra_data;
    }

    /**
     * @param mixed $description
     */
    public function set_description($description): void
    {
        $this->description = $description;
    }

    public function set_proxy($proxy): void
    {
        $this->proxy = $proxy;
    }

    public function get_proxy(): string
    {
        return $this->proxy;
    }
    public function get_regular_price() : ?float
    {
        return $this->regular_price;
    }

    public function get_sales_price()
    {
        return $this->sales_price;
    }
    public function get_price_billet()
    {
        return $this->price_billet;
    }
    public function get_offer_type()
    {
        return $this->offer_type;
    }
    public function get_price_offer()
    {
        return $this->price_offer;
    }
    public function get_installments_price()
    {
        return $this->installments_price;
    }
    public function get_installments()
    {
        return $this->installments;
    }
    public function get_installment_amount()
    {
        return $this->installment_amount;
    }
    public function get_interest_rate()
    {
        return $this->interest_rate;
    }
    public function get_stock()
    {
        return $this->stock;
    }
    public function get_stock_quantity()
    {
        return $this->stock_quantity;
    }
    public function get_status()
    {
        return $this->status;
    }
    public function get_obs()
    {
        return $this->obs;
    }


    public function set_regular_price(float $regular_price)
    {
        $this->regular_price = $regular_price;
    }

    public function set_sales_price(float $sales_price)
    {
        $this->sales_price = $sales_price;
    }

    public function set_price_billet(float $price_billet)
    {
        $this->price_billet = $price_billet;
    }

    public function set_offer_type($offer_type)
    {
        $this->offer_type = $offer_type;
    }

    public function set_price_offer(float $price_offer)
    {
        $this->price_offer = $price_offer;
    }

    public function set_installments_price(float $installments_price)
    {
        $this->installments_price = $installments_price;
    }
    public function set_installments(int $installments)
    {
        $this->installments = $installments;
    }
    public function set_installment_amount(float $installment_amount)
    {
        $this->installment_amount = $installment_amount;
    }
    public function set_interest_rate(string $interest_rate)
    {
        $this->interest_rate = $interest_rate;
    }
    public function set_stock($stock)
    {
        $this->stock = $stock;
    }
    public function set_stock_quantity(int $stock_quantity)
    {
        $this->stock_quantity = $stock_quantity;
    }
    public function set_status($status)
    {
        $this->status = $status;
    }
    public function set_obs(string $obs)
    {
        $this->obs = $obs;
    }

    /**
     * @return int
     */
    public function get_unit(): int
    {
        return $this->unit;
    }

    /**
     * @param int $unit
     */
    public function set_unit(int $unit): void
    {
        $this->unit = $unit;
    }

    /**
     * @return mixed
     */
    public function get_extra_data()
    {
        return $this->extra_data;
    }

    /**
     * @param mixed $extra_data
     */
    public function set_extra_data($extra_data): void
    {
        $this->extra_data = $extra_data;
    }



    public function jsonSerialize()
    {
        return [
            'sku' => $this->get_sku(),
            'url' => $this->get_url(),
            'brand' => $this->get_brand(),
            'description' => $this->get_description(),
            'ean' => $this->get_ean(),
            'regular_price' => $this->get_regular_price(),
            'sales_price' => $this->get_sales_price(),
            'price_billet' => $this->get_price_billet(),
            'offer_type' => $this->get_offer_type(),
            'price_offer' => $this->get_price_offer(),
            'installments_price' => $this->get_installments_price(),
            'installments' => $this->get_installments(),
            'installment_amount' => $this->get_installment_amount(),
            'interest_rate' => $this->get_interest_rate(),
            'stock' => $this->get_stock(),
            'stock_quantity' => $this->get_stock_quantity(),
            'status' => $this->get_status(),
            'obs' => $this->get_obs(),
            'model' => $this->get_model(),
            'seller' => $this->get_seller(),
            'object' => $this->get_object(),
            'types' => $this->get_types(),
            'unit' => $this->get_unit(),
            'extra' => $this->get_extra(),
            'image' => $this->get_image(),
        ];
    }
}
