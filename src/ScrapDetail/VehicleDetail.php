<?php


namespace Eiprice\Core\ScrapDetail;


use Eiprice\Core\Contract\IDataCrawler;

/**
 * Class VehicleDetail
 * @package Eiprice\Core\ScrapDetail
 */
class VehicleDetail implements IDataCrawler
{
    /**
     * @var string
     */
    protected $slug = '';

    /**
     * @var array
     */
    protected $slugs = [];

    /**
     * @var string
     */
    protected $image = '';

    /**
     * @var string
     */
    protected $id = '';
    /**
     * @var string $brand
     */
    protected $brand = '';


    /**
     * @var string
     */
    protected $title = '';

    /**
     * @var string
     */
    protected $object = '';

    /**
     * @var string
     */
    protected $proxy = '';

    /**
     * @var string $model
     */
    protected $model = '';

    /**
     * @var int $modelYear
     */
    protected $modelYear;

    /**
     * @var int $manufactureYear
     */
    protected $manufactureYear;

    /**
     * @var string
     */
    protected $version = '';

    /**
     * @var string $color
     */
    protected $color = '';

    /**
     * @var float
     */
    protected $price = 0.00;

    /**
     * @var float
     */
    protected $standardPrice = 0.00;


    protected $averagePrice = 0.00;

    /**
     * @var int
     */
    protected $km = 0;

    /**
     * @var bool $armored
     */
    protected $armored;

    /**
     * @var int|null
     */
    protected $numberOfDoors = null;


    /**
     * @var bool|null $oneOwner
     */
    protected $oneOwner = null;

    /**
     * @var bool|null $ipva
     */
    protected $ipva = null;

    /**
     * @var bool|null $acceptChange
     */
    protected $acceptChange = null;


    /**
     * @var bool|null
     */
    protected $financed = null;

    /**
     * @var bool|null $licensed
     */
    protected $licensed = null;

    /**
     * @var bool|null $factoryWarranty
     */
    protected $factoryWarranty = null;

    /**
     * @var bool|null $collector
     */
    protected $collector = null;

    /**
     * @var bool|null $allReviews
     */
    protected $allReviews = null;

    /**
     * @var bool|null $disabledAdapted
     */
    protected $disabledAdapted = null;

    /**
     * @var null
     */
    protected $sellerID = null;

    /**
     * @var null
     */
    protected $sellerType = null;

    /**
     * @var null
     */
    protected $sellerCity = null;

    /**
     * @var null
     */
    protected $sellerState = null;

    /**
     * @var string
     */
    protected $transmission;

    /**
     * @var
     */
    protected $bodyType;

    /**
     * @var array
     */
    protected $vehicleAttributes = [];

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @param string $id
     */
    public function setId(string $id): void
    {
        $this->id = $id;
    }

    /**
     * @return float
     */
    public function getStandardPrice(): float
    {
        return $this->standardPrice;
    }

    /**
     * @param float $standardPrice
     */
    public function setStandardPrice(float $standardPrice): void
    {
        $this->standardPrice = $standardPrice;
    }

    /**
     * @return float
     */
    public function getAveragePrice(): float
    {
        return $this->averagePrice;
    }

    /**
     * @param float $averagePrice
     */
    public function setAveragePrice(float $averagePrice): void
    {
        $this->averagePrice = $averagePrice;
    }

    /**
     * @return null
     */
    public function getSellerID()
    {
        return $this->sellerID;
    }

    /**
     * @param null $sellerID
     */
    public function setSellerID($sellerID): void
    {
        $this->sellerID = $sellerID;
    }

    /**
     * @return string
     */
    public function getTransmission(): string
    {
        return $this->transmission;
    }

    /**
     * @param string $transmission
     */
    public function setTransmission(string $transmission): void
    {
        $this->transmission = $transmission;
    }

    /**
     * @return mixed
     */
    public function getBodyType()
    {
        return $this->bodyType;
    }

    /**
     * @param mixed $bodyType
     */
    public function setBodyType($bodyType): void
    {
        $this->bodyType = $bodyType;
    }

    /**
     * @return mixed
     */
    public function getVehicleAttributes()
    {
        return $this->vehicleAttributes;
    }

    /**
     * @param mixed $vehicleAttributes
     */
    public function setVehicleAttributes($vehicleAttributes): void
    {
        $this->vehicleAttributes = $vehicleAttributes;
    }






    /**
     * @return string
     */
    public function getImage(): string
    {
        return $this->image;
    }

    /**
     * @param string $image
     */
    public function setImage(string $image): void
    {
        $this->image = $image;
    }

    /**
     * @return string
     */
    public function getSlug(): string
    {
        return $this->slug;
    }

    /**
     * @return array
     */
    public function getSlugs(): array
    {
        return $this->slugs;
    }

    /**
     * @param array $slugs
     */
    public function setSlugs(array $slugs): void
    {
        $this->slugs = $slugs;
    }



    /**
     * @param string $slug
     */
    public function setSlug(string $slug): void
    {
        $this->slug = $slug;
    }


    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle(string $title): void
    {
        $this->title = $title;
    }

    /**
     * @return null
     */
    public function getSellerType()
    {
        return $this->sellerType;
    }

    /**
     * @param null $sellerType
     */
    public function setSellerType($sellerType): void
    {
        $this->sellerType = $sellerType;
    }

    /**
     * @return null
     */
    public function getSellerCity()
    {
        return $this->sellerCity;
    }

    /**
     * @param null $sellerCity
     */
    public function setSellerCity($sellerCity): void
    {
        $this->sellerCity = $sellerCity;
    }

    /**
     * @return null
     */
    public function getSellerState()
    {
        return $this->sellerState;
    }

    /**
     * @param null $sellerState
     */
    public function setSellerState($sellerState): void
    {
        $this->sellerState = $sellerState;
    }




    /**
     * @return string
     */
    public function getBrand(): string
    {
        return $this->brand;
    }

    /**
     * @param string $brand
     */
    public function setBrand(string $brand): void
    {
        $this->brand = $brand;
    }

    /**
     * @return string
     */
    public function getModel(): string
    {
        return $this->model;
    }

    /**
     * @param string $model
     */
    public function setModel(string $model): void
    {
        $this->model = $model;
    }

    /**
     * @return int
     */
    public function getModelYear(): ?int
    {
        return $this->modelYear;
    }

    /**
     * @param int $modelYear
     */
    public function setModelYear(int $modelYear): void
    {
        $this->modelYear = $modelYear;
    }

    /**
     * @return int
     */
    public function getManufactureYear(): ?int
    {
        return $this->manufactureYear;
    }

    /**
     * @param int $manufactureYear
     */
    public function setManufactureYear(int $manufactureYear): void
    {
        $this->manufactureYear = $manufactureYear;
    }

    /**
     * @return string
     */
    public function getVersion(): string
    {
        return $this->version;
    }

    /**
     * @param string $version
     */
    public function setVersion(string $version): void
    {
        $this->version = $version;
    }

    /**
     * @return string
     */
    public function getColor(): string
    {
        return $this->color;
    }

    /**
     * @param string $color
     */
    public function setColor(string $color): void
    {
        $this->color = $color;
    }

    /**
     * @return float
     */
    public function getPrice(): float
    {
        return $this->price;
    }

    /**
     * @param float $price
     */
    public function setPrice(float $price): void
    {
        $this->price = $price;
    }

    /**
     * @return int
     */
    public function getKm(): int
    {
        return $this->km;
    }

    /**
     * @param int $km
     */
    public function setKm(int $km): void
    {
        $this->km = $km;
    }

    /**
     * @return bool
     */
    public function isArmored(): bool
    {
        return $this->armored;
    }

    /**
     * @param bool $armored
     */
    public function setArmored(bool $armored): void
    {
        $this->armored = $armored;
    }

    /**
     * @return int|null
     */
    public function getNumberOfDoors(): ?int
    {
        return $this->numberOfDoors;
    }

    /**
     * @param int|null $numberOfDoors
     */
    public function setNumberOfDoors(?int $numberOfDoors): void
    {
        $this->numberOfDoors = $numberOfDoors;
    }

    /**
     * @return bool|null
     */
    public function getOneOwner(): ?bool
    {
        return $this->oneOwner;
    }

    /**
     * @param bool|null $oneOwner
     */
    public function setOneOwner(?bool $oneOwner): void
    {
        $this->oneOwner = $oneOwner;
    }

    /**
     * @return bool|null
     */
    public function getIpva(): ?bool
    {
        return $this->ipva;
    }

    /**
     * @param bool|null $ipva
     */
    public function setIpva(?bool $ipva): void
    {
        $this->ipva = $ipva;
    }

    /**
     * @return bool|null
     */
    public function getAcceptChange(): ?bool
    {
        return $this->acceptChange;
    }

    /**
     * @param bool|null $acceptChange
     */
    public function setAcceptChange(?bool $acceptChange): void
    {
        $this->acceptChange = $acceptChange;
    }

    /**
     * @return bool|null
     */
    public function getFinanced(): ?bool
    {
        return $this->financed;
    }

    /**
     * @param bool|null $financed
     */
    public function setFinanced(?bool $financed): void
    {
        $this->financed = $financed;
    }

    /**
     * @return bool|null
     */
    public function getLicensed(): ?bool
    {
        return $this->licensed;
    }

    /**
     * @param bool|null $licensed
     */
    public function setLicensed(?bool $licensed): void
    {
        $this->licensed = $licensed;
    }

    /**
     * @return bool|null
     */
    public function getFactoryWarranty(): ?bool
    {
        return $this->factoryWarranty;
    }

    /**
     * @param bool|null $factoryWarranty
     */
    public function setFactoryWarranty(?bool $factoryWarranty): void
    {
        $this->factoryWarranty = $factoryWarranty;
    }

    /**
     * @return bool|null
     */
    public function getCollector(): ?bool
    {
        return $this->collector;
    }

    /**
     * @param bool|null $collector
     */
    public function setCollector(?bool $collector): void
    {
        $this->collector = $collector;
    }

    /**
     * @return bool|null
     */
    public function getAllReviews(): ?bool
    {
        return $this->allReviews;
    }

    /**
     * @param bool|null $allReviews
     */
    public function setAllReviews(?bool $allReviews): void
    {
        $this->allReviews = $allReviews;
    }

    /**
     * @return bool|null
     */
    public function getDisabledAdapted(): ?bool
    {
        return $this->disabledAdapted;
    }

    /**
     * @param bool|null $disabledAdapted
     */
    public function setDisabledAdapted(?bool $disabledAdapted): void
    {
        $this->disabledAdapted = $disabledAdapted;
    }

    public function set_proxy($proxy): void
    {
        $this->proxy = $proxy;
    }

    public function get_proxy(): string
    {
        return $this->proxy;
    }

    public function setObject($object)
    {
        $this->object = $object;
    }

    public function getObject()
    {
        return $this->object;
    }

    public function jsonSerialize()
    {

        return [
            'slug' => $this->getSlug(),
            'slugs' => $this->getSlugs(),
            'image' => $this->getImage(),
            'id' => $this->getId(),
            'brand' => $this->getBrand(),
            'title' => $this->getTitle(),
            'object' => $this->getObject(),
            'proxy' => $this->get_proxy(),
            'model' => $this->getModel(),
            'modelYear' => $this->getModelYear(),
            'manufactureYear' => $this->getManufactureYear(),
            'version' => $this->getVersion(),
            'color' => $this->getColor(),
            'price' => $this->getPrice(),
            'standardPrice' => $this->getStandardPrice(),
            'averagePrice' => $this->getAveragePrice(),
            'km' => $this->getKm(),
            'armored' => $this->isArmored(),
            'numberOfDoors' => $this->getNumberOfDoors(),
            'oneOwner' => $this->getOneOwner(),
            'ipva' => $this->getIpva(),
            'acceptChange' => $this->getAcceptChange(),
            'financed' => $this->getFinanced(),
            'licensed' => $this->getLicensed(),
            'factoryWarranty' => $this->getFactoryWarranty(),
            'collector' => $this->getCollector(),
            'allReviews' => $this->getAllReviews(),
            'disabledAdapted' => $this->getDisabledAdapted(),
            'sellerID' => $this->getSellerID(),
            'sellerType' => $this->sellerType(),
            'sellerCity' => $this->getSellerCity(),
            'sellerState' => $this->getSellerState(),
            'transmission' => $this->getTransmission(),
            'bodyType' => $this->getBodyType(),
            'vehicleAttributes' => $this->getVehicleAttributes(),
        ];
    }
}
