<?php


namespace Eiprice\Core\Search;


/**
 * Class ProductSearch
 * @package Bot\Eiprice\Core
 */
class ProductSearch
{
    protected $ean;
    protected $sku;
    protected $slug;
    protected $model;
    protected $brand;
    protected $atributo;

    /**
     * @param array $data
     * @return ProductSearch
     */
    public static function fromArray(array $data)
    {
        $instance = new self;
        $instance->set_model(isset($data['model'])?$data['model']: null );
        $instance->set_ean(isset($data['ean'])?$data['ean']: null );
        $instance->set_sku(isset($data['sku'])?$data['sku']: null );
        $instance->set_slug(isset($data['slug'])?$data['slug']: null );
        $instance->set_atributo(isset($data['attribute'])?$data['attribute']: null );

        return $instance;
    }

    /**
     * @return mixed
     */
    public function get_ean()
    {
        return $this->ean;
    }

    /**
     * @param mixed $ean
     */
    public function set_ean($ean): void
    {
        $this->ean = $ean;
    }

    /**
     * @return mixed
     */
    public function get_sku()
    {
        return $this->sku;
    }

    /**
     * @param mixed $sku
     */
    public function set_sku($sku): void
    {
        $this->sku = $sku;
    }

    /**
     * @return mixed
     */
    public function get_tipo()
    {
        return $this->tipo;
    }

    /**
     * @param mixed $tipo
     */
    public function set_tipo($tipo): void
    {
        $this->tipo = $tipo;
    }

    /**
     * @return mixed
     */
    public function get_descricao()
    {
        return $this->descricao;
    }

    /**
     * @param mixed $descricao
     */
    public function setDescricao($descricao): void
    {
        $this->descricao = $descricao;
    }

    /**
     * @return mixed
     */
    public function getColeta()
    {
        return $this->coleta;
    }

    /**
     * @param mixed $coleta
     */
    public function setColeta($coleta): void
    {
        $this->coleta = $coleta;
    }

    /**
     * @return mixed
     */
    public function get_slug()
    {
        return $this->slug;
    }

    /**
     * @param mixed $slug
     */
    public function set_slug($slug): void
    {
        $this->slug = $slug;
    }

    /**
     * @return mixed
     */
    public function get_atributo()
    {
        return $this->atributo;
    }

    /**
     * @param mixed $atributo
     */
    public function set_atributo($atributo): void
    {
        $this->atributo = $atributo;
    }

    /**
     * @return mixed
     */
    public function get_model()
    {
        return $this->model;
    }

    /**
     * @param mixed $model
     */
    public function set_model($model): void
    {
        $this->model = $model;
    }

    /**
     * @return mixed
     */
    public function get_imagem()
    {
        return $this->imagem;
    }

    /**
     * @param mixed $imagem
     */
    public function set_imagem($imagem): void
    {
        $this->imagem = $imagem;
    }
}
