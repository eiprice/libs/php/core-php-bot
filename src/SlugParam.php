<?php


namespace Eiprice\Core;


use Eiprice\Core\Contract\ISpiderContainer;

/**
 * Class SlugParam
 * @package Eiprice\Core
 */
class SlugParam
{
    protected $url;
    protected $container;

    public function set_url($url)
    {
        $this->url = $url;
    }

    public function set_container($container)
    {
        $this->container = $container;
    }

    public function get_url()
    {
        return $this->url;
    }

    public function get_container() : ISpiderContainer
    {
        return $this->container;
    }
}
