<?php


namespace Eiprice\Core\Spiders;

use Eiprice\Core\Contract\IDataSpider;
use Eiprice\Core\Contract\ISpider;
use Eiprice\Core\Contract\ISpiderContainer;
use Eiprice\Core\Search\ProductSearch;
use Eiprice\Core\ScrapDetail\ProductDetail;
use Eiprice\Core\Spiders\Traits\HasDrivers;
use Eiprice\Core\Spiders\Traits\HasRequests;
use Psr\Log\LoggerInterface;
use \Closure;
use \ReflectionException;
use \ReflectionFunction;
use \ReflectionMethod;
use System\Exceptions\SlugNotFound;

use Eiprice\Core\SlugParam;


/**
 * Class Spider
 * @package Bot\Eiprice\Core
 */
abstract class BasicSpider implements ISpider
{
    use HasDrivers, HasRequests;

    /**
     * @var LoggerInterface
     */
    protected $logger;

    /**
     * @var Closure
     */
    protected $onProductDetail;

    /**
     * @var Closure
     */
    protected $onSlugFoundDetail;

    /**
     * @var Closure
     */
    protected $onProductNotFound;

    /**
     * @var Closure
     */
    protected $onMultiple;

    /**
     * @var ProductSearch
     */
    protected $o_pesquisa;


    /**
     * @return string
     */
    abstract public function get_base_url() : string;

    /**
     * @return array
     */
    abstract public function get_headers() : array;

    /**
     * @return string
     */
    abstract public function get_url_by_slug() : string;


    /**
     * @return string
     */
    abstract public function get_url_by_sku() : string;

    /**
     * @param ISpiderContainer $container
     * @return bool
     */
    abstract protected function check_product_not_found(ISpiderContainer $container) : bool ;

    /**
     * @param ISpiderContainer $container
     * @return bool
     */
    abstract protected function check_product_found(ISpiderContainer $container) : bool ;

    /**
     * @param ISpiderContainer $container
     * @param $totalItens
     * @return bool
     */
    abstract protected function check_result(ISpiderContainer $container, &$totalItens) : bool;


    /**
     * @return array
     */
    public function custom_headers() : array
    {
        return [];
    }



    /**
     * @param ISpiderContainer $container
     * @throws SlugNotFound
     * @return string
     */
    abstract protected function extract_slug(ISpiderContainer $container) : string;

    /**
     * @param SlugParam $a_data
     * @return ProductDetail
     */
    abstract protected function extract_detail(SlugParam $a_data) : IDataSpider;

    /**
     *
     */
    protected function boot() : void
    {
        //DO NOTHING
    }

    /**
     *
     */
    protected function init() : void
    {
        //DO NOTHING
    }


    /**
     * @param callable $callable
     * @throws ReflectionException
     */
    final public function setOnProductDetail(callable $callable) : void
    {
        if ( is_array($callable)){
            $refFunc = new ReflectionMethod($callable[0], $callable[1]);
        }else{
            $refFunc = new ReflectionFunction( $callable);
        }

        $params = $refFunc->getParameters();
        $first_param = reset($params);
        if ( $first_param->getType() !== ProductDetail::class){
            // TODO excpetion
        }
        $this->onProductDetail = $callable;
    }

    /**
     * @param callable $callable
     * @throws ReflectionException
     */
    final public function setOnSlugFound(callable $callable) : void
    {
        if ( is_array($callable)){
            $refFunc = new ReflectionMethod($callable[0], $callable[1]);
        }else{
            $refFunc = new ReflectionFunction( $callable);
        }

        $params = $refFunc->getParameters();
        $first_param = reset($params);
        if ( $first_param->getType() !== 'string'){
            // TODO excpetion
        }

        $this->onSlugFoundDetail = $callable;
    }

    /**
     * @param callable $callable
     * @throws ReflectionException
     */
    final public function setOnProductNotFound(callable $callable) : void
    {
        if ( is_array($callable)){
            $refFunc = new ReflectionMethod($callable[0], $callable[1]);
        }else{
            $refFunc = new ReflectionFunction( $callable);
        }


        $this->logger->info('Updating onProductNotFound', [$callable]);

        $this->onProductNotFound = $callable;
    }

    /**
     * @param callable $callable
     * @throws ReflectionException
     */
    final public function setOnMultiple(callable $callable) : void
    {
        if ( is_array($callable)){
            $refFunc = new ReflectionMethod($callable[0], $callable[1]);
        }else{
            $refFunc = new ReflectionFunction( $callable);
        }


        $this->logger->info('Updating onMultiple', [$callable]);

        $this->onMultiple = $callable;
    }

    final function __construct(LoggerInterface $logger)
    {
        $this->logger = $logger;
        $this->init();
        $this->boot();

        $this->onProductDetail = function (ProductDetail $product){
            $this->logger->info('Closure onProductDetail not implemented', [$product]);
        };

        $this->onSlugFoundDetail = function (string $slug){
            $this->logger->info('Closure onSlugFoundDetail not implemented', [$slug]);
        };

        $this->onProductNotFound = function (){
            $this->logger->info('Closure onProductNotFound not implemented');
        };

        $this->onMultiple = function (){
            $this->logger->info('Closure onMultiple not implemented');
        };
    }

    /**
     * @return PesquisaProduto
     * @throws \Exception
     */
    final public function get_o_pesquisa()
    {
        if ( empty($this->o_pesquisa)){
            throw new \Exception("Pesquisa not setted");
        }

        return $this->o_pesquisa;
    }

    /**
     * @param ProductSearch $o_pesquisa
     * @throws \Exception
     */
    final public function set_o_pesquisa(ProductSearch $o_pesquisa): void
    {
        $this->o_pesquisa = $o_pesquisa;
    }




    /**
     * {@inheritdoc}
     */
    final public function check_step(ISpiderContainer $container): void
    {
        $this->logger->debug('CheckStep');

        $totalItens = 0;


        if($this->check_product_found($container)) {
            $this->logger->info("Product page found", [$container->get_url()]);
            $param = new SlugParam();
            $param->set_url($container->get_url());
            $param->set_container($container);
            $product_detail = $this->extract_detail($param);

            call_user_func_array($this->onProductDetail, [$product_detail]);

        } elseif ($this->check_result($container, $totalItens)) {
            $this->logger->debug('check_result');
            if ($totalItens == 1) {
                $this->logger->info("The search has returned 1 result", [$container->get_url()]);

                $slug = $this->extract_slug($container);
                call_user_func_array($this->onSlugFoundDetail, [$slug]);
            } else {
                log_info("The search has returned multiples results", [$container->get_url()]);

                call_user_func_array($this->onMultiple, []);
            }
        } elseif (!$this->check_product_found($container)) {
            $this->logger->debug('not_found');
            $this->logger->info("Product nof tound", [$container->get_url()]);
            call_user_func_array($this->onProductNotFound, []);
        } else {
            $this->logger->info("Unexpected situation", [$container->get_url()]);
        }
    }
}
