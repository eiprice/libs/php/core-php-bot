<?php


namespace Eiprice\Core\Spiders\Crawler;

use Eiprice\Core\Contract\IDataCrawler;
use Eiprice\Core\Contract\ISpiderContainer;
use Eiprice\Core\Contract\IDataSpider;
use Eiprice\Core\SlugParam;
use Psr\Log\LoggerInterface;
use stdClass;

/**
 * Class Piece
 * @package Eiprice\Core\Spiders\Crawler
 */
abstract class PageProcessor
{
    /**
     * @var stdClass|null
     */
    protected $data = null;
    protected $url = null;

    /**
     * @var LoggerInterface
     */
    protected $logger;

    /**
     * @var string $spiderName
     */
    private $spiderName;

    public function __construct(LoggerInterface $logger, $spiderName)
    {
        $this->logger = $logger;
        $this->spiderName = $spiderName;
    }

    protected function getSpiderName()
    {
        return $this->spiderName;
    }

    /**
     * @var ISpiderContainer $container
     */
    protected $container;

    public function setup(ISpiderContainer $container): void
    {
        $this->data = $container->json();
        $this->container = $container;
    }


    /**
     * @param ISpiderContainer $container
     * @return bool
     */
    abstract public function check_condiction() : bool;

    /**
     * @return IDataCrawler
     */
    abstract public function extract_detail() : ?IDataCrawler;


    /**
     * @return array
     */
    abstract public function extract_slugs() : array;
}
