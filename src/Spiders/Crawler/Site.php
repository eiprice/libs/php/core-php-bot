<?php

namespace Eiprice\Core\Spiders\Crawler;


use Eiprice\Core\Collection\DataCrawlerCollection;
use Eiprice\Core\Collection\PageProcessorCollection;
use Eiprice\Core\Contract\ISpider;
use Eiprice\Core\Contract\ISpiderContainer;
use Eiprice\Core\Spiders\Traits\HasCustomHeaders;
use Eiprice\Core\Spiders\Traits\HasDrivers;
use Eiprice\Core\Spiders\Traits\HasRequests;
use Eiprice\Webdriver\GuzzleDriver;

/**
 * Class Site
 * @package Eiprice\Core\Spiders\Crawler
 */
abstract class Site implements ISpider
{
    use HasDrivers, HasRequests, HasCustomHeaders;

    const BASE_URL = self::BASE_URL;

    /**
     * @var string$host
     */
    protected $host;

    /***
     * @var array
     */
    protected $PageProcessors = [];

    /**
     * @var PageProcessorCollection|PageProcessor[] $PageProcessorCollection
     */
    private $PageProcessorCollection;

    public function available_drivers(): array
    {
        return [
            GuzzleDriver::class
        ];
    }

    public function getName() {

        return substr(strrchr(get_class($this), "\\"), 1);
    }

    /**
     * Site constructor.
     */
    public function __construct()
    {
        //
        $this->PageProcessorCollection = new PageProcessorCollection();

        //
        foreach ($this->PageProcessors as $PageProcessor){
            $this->PageProcessorCollection->add(
                app()->make($PageProcessor, ['spiderName' => get_class($this) ] )
            );
        }
    }

    /**
     *
     *
     * @return PageProcessorCollection|PageProcessor[]
     */
    public function getPageProcessors() : PageProcessorCollection
    {
        return $this->PageProcessorCollection;
    }

    public function process(ISpiderContainer $container) : DataCrawlerCollection
    {
        $collection = new DataCrawlerCollection();

        foreach ($this->getPageProcessors() as $processor){

            $processor->setup($container);

            if ( $processor->check_condiction()){
                while($data = $processor->extract_detail() ){
                    $data->setSlugs($processor->extract_slugs());

                    $collection[] = $data;
                }
            }
        }

        return $collection;
    }
}
