<?php

namespace Eiprice\Core\Spiders\Product;

use Eiprice\Core\Spiders\Shipment;

/**
 * Class WithShipment
 * @package Eiprice\Core\Spiders\Product
 */
abstract class WithShipment
    extends SimpleSpider
{
    const SHIPMENT_CLASS = self::SHIPMENT_CLASS;

    /**
     * @var Shipment[] $shippmentCollection
     */
    protected $shippmentCollection;


    /**
     * @var Shipment|string|array $shipment
     */
    public function addShipment($shipment) : void
    {
        $class = static::SHIPMENT_CLASS;

        if ( ! is_object($shipment)){
            $shipment = new $class($shipment);
        }

        $this->shippmentCollection->add($shipment);
    }

    final protected function init(): void
    {
        $this->shippmentCollection = new ShipmentSpiderCollection();
    }

    /**
     * @return Shipment[]
     */
    public function getShippmentCollection()
    {
        return $this->shippmentCollection;
    }
}
