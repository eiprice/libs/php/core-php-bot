<?php


namespace Eiprice\Core\Spiders;


use Eiprice\Core\Collection\ShipmentDetailCollection;
use Eiprice\Core\Contract\ISpiderContainer;

abstract class Shipment
{
    protected $postal_code = '';

    /**
     * Shipment constructor.
     * @param $postal_code
     */
    public function __construct($postal_code)
    {
        $this->postal_code = $postal_code;
    }

    /**
     * @return array
     */
    public function pre_requests() : array
    {
        return [];
    }


    abstract function last_url() : string;

    abstract function get_details(ISpiderContainer $container) : ShipmentDetailCollection;
}
