<?php


namespace Eiprice\Core\Spiders\Traits;


trait HasCustomHeaders
{
    /**
     * @return array
     */
    public function custom_headers() : array
    {
        return [];
    }
}
