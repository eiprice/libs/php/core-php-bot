<?php

namespace Eiprice\Core\Spiders\Traits;

trait HasDrivers
{
    /**
     * @return array
     */
    public function available_drivers() : array
    {
        return [
            \Eiprice\Webdriver\EiproxyDriver::class,
        ];
    }

}
