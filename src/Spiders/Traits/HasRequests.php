<?php


namespace Eiprice\Core\Spiders\Traits;


trait HasRequests
{
    /**
     * @return array
     */
    public function pre_requests() : array
    {
        return [];
    }

    /**
     * @return array
     */
    public function post_requests() : array
    {
        return [];
    }
}
